<?php
Load::models('usuario');
class InfoUsuario extends ActiveRecord
{
	public function nuevo($infoUsuario)
	{
		try {
			$this->save($infoUsuario);
		} catch (Exception $e) {
			Flash::error('Error al crear el registro');
			Logger::error($e->getMessage());
			return FALSE;
		}
	}
	
	public function after_save()
	{
		$usuario = new Usuario();
		$usuario->user_info_id = $this->id;
		try {
			$usuario->save(Input::post('usuario'));
			return TRUE;
		} catch (Exception $e) {
			$this->delete();
			Logger::error($e->getMessage());
			return FALSE;
		}
	}
	
	public function getInfo($id)
	{
		return $this->find($id);
	}
}