<?php
/**
 * Clase que conecta con Digest para generar valor único
 */
class Digest
{
	/**
	 * URL para generar el Digest
	 * @var unknown_type
	 */
	//private static $_serviceDigest = 'http://sportpark.com.ve:8080/Digest_v1/digest/71366987/0/4001/%s/937/%s';
	private static $_serviceDigest = 'http://localhost:8080/Digest_v1/digest/71366987/0/4001/%s/937/%s';
	/**
	 * URL para verificar respuesta del banco
	 * @var unknown_type
	 */
	//private static $_serviceDigestResponse = 'http://sportpark.com.ve:8080/Digest_v1/digest/71366987/0/4001/%s/%s/%s';
	private static $_serviceDigestResponse = 'http://localhost:8080/Digest_v1/digest/71366987/0/4001/%s/%s/%s';

	private static function _connect($url)
	{
		$c = curl_init($url);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($c, CURLOPT_HEADER, 0);
        $data = curl_exec($c);
        curl_close($c);
        return $data;
	}
	/**
	 * Devuelve el Digest
	 * @param $total
	 * @param $orderID
	 * @return string
	 */
	public static function getDigest($total, $orderID)
	{
		$total = number_format($total, 2, '', '');
		$digest = new SimpleXMLElement(self::_connect(sprintf(self::$_serviceDigest, $total, $orderID)));
		return $digest;
	}
	public static function getDigest2($total, $orderID)
	{
		var_dump([self::$_serviceDigest, $total, $orderID]);
		
		$digest = new SimpleXMLElement(self::_connect(sprintf(self::$_serviceDigest, $total, $orderID)));
		var_dump($digest);
		return $digest;
	}
	public static function getDigestResponse($total, $refnum, $orderID)
	{
		$digest = new SimpleXMLElement(self::_connect(sprintf(self::$_serviceDigestResponse, $total, $refnum, $orderID)));
        return $digest;
	}
}
