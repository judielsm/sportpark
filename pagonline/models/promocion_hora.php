<?php
class PromocionHora extends ActiveRecord
{
	public function initializer(){
		$this->belongs_to('promocion');
	}
	/**
	 * Obtiene las horas de un dia de promocion
	 * esto porque en los dias la cantidad de hora varia
	 * @param $dia
	 * @return array horas
	 */
	public function getHoras($dia=7)
	{
		$sql = "SELECT DISTINCT(h.hora) as hora, h.id, p.precio, h.hora_time FROM promocion p
			    INNER JOIN promocion_hora as ph ON ph.promocion_id=p.id
			    INNER JOIN promocion_dia as pd ON pd.promocion_id=p.id
			    INNER JOIN promocion_cancha as pc ON pc.promocion_id=p.id
			    INNER JOIN dia as d ON d.id = pd.dia_id
			    INNER JOIN hora as h ON h.id = ph.hora_id
			    INNER JOIN cancha as c ON c.id = pc.cancha_id
			    WHERE p.estatus=1 AND d.id=$dia order by h.id ASC";
		return $this->find_all_by_sql($sql);
	}
}
/*
   SELECT DISTINCT(h.hora), p.nombre, p.precio, p.id as "promocion_id", d.nombre, c.nombre FROM promocion_hora ph
   LEFT JOIN promocion as p ON p.id = ph.promocion_id
   LEFT JOIN promocion_dia as pd ON pd.promocion_id = ph.promocion_id
   LEFT JOIN promocion_cancha as pc ON pc.promocion_id = ph.promocion_id
   LEFT JOIN hora as h ON h.id = ph.hora_id
   LEFT JOIN dia as d ON d.id = pd.dia_id
   LEFT JOIN cancha as c ON c.id = pc.cancha_id
   WHERE d.id=1 AND c.id=1 AND p.estatus=1
 */