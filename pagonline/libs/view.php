<?php
/**
 * Esta clase permite extender o modificar la clase ViewBase de Kumbiaphp.
 *
 * @category KumbiaPHP
 * @package View
 **/

// @see KumbiaView
require_once CORE_PATH . 'kumbia/kumbia_view.php';

class View extends KumbiaView {
    /**
     * Obtiene el contenido de un view
     */
    public static function bufferedPartial ($partial, $params = array())
    {
        ob_start();
        self::partial($partial, FALSE, $params);
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

}
