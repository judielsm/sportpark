<?php
/**
 * Todas las controladores heredan de esta clase en un nivel superior
 * por lo tanto los metodos aqui definidos estan disponibles para
 * cualquier controlador.
 *
 * @category Kumbia
 * @package Controller
 **/

// @see Controller nuevo controller
require_once CORE_PATH . 'kumbia/controller.php';

class AppController extends Controller {

	final protected function initialize() {
		Load::lib('SdAuth');
		if (SdAuth::isLogged ()) {
			$this->rol = Session::get('rol');
			$this->user =  Session::get('userID');
			if($this->module_name == 'admin' && $this->rol != 'admin'){
				$user = Session::get('userID');
				
				Logger::warning("USUARIO INTENTANDO INGRESAR SIN PERMISO|$user", 'access-fail');
				return Router::redirect('/');
			}
			View::template ( 'default' );
		} else {
			if($this->controller_name != 'login' && ($this->action_name != 'registro' || $this->action_name != 'recuperar_clave' || $this->action_name != 'registro')){
				$this->error_msj = SdAuth::getError ();
				View::select (NULL, 'login' );
				//View::select(NULL, 'new_template');
				return FALSE;
			}
		}
	}
	
	final protected function finalize() {
	}
	
    public function logout () {
        Load::lib('SdAuth');
        SdAuth::logout();
        View::select(NULL, 'login');
        return Router::redirect('/');
    }
}