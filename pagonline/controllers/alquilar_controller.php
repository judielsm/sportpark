<?php
Load::models('alquiler', 'digest', 'transaccion', 'mailer');
class AlquilarController extends AppController
{
  public $fecha_reservacion = NULL;
  public function index()
  {
    $date = new DateTime('now');
    Session::set('fecha_reserva', $date->format('d-m-Y'));
    if(Input::hasPost('fecha')){
      Session::set('fecha_reserva', Input::post('fecha'));
      return Router::toAction('myList');
    }
  }
  public function myList()
  {
    View::select('list');
  }
  public function listToday()
  {
    if(Session::has('fecha_reserva')){
      Session::delete('fecha_reserva');
    }
    View::select('list');
  }
  public function paint()
  {
 
   if(!Session::has('fecha_reserva')){
      $date = new DateTime('now');
            $this->fecha_reservacion = $date->format('d-m-Y');
    }else{
      $this->fecha_reservacion = Session::get('fecha_reserva');
    }
    View::select('disponibilidad-paint');
  }
  /**
   * Muestra la disponibilidad de las canchas
   * 
   */
  public function disponibilidad()
  {
    if(!Session::has('fecha_reserva')){
      $date = new DateTime('now');
            $this->fecha_reservacion = $date->format('d-m-Y');
    }else{
      $this->fecha_reservacion = Session::get('fecha_reserva');
    }
    
    if(Input::hasPost('reservacion')){
      if($this->rol == 'admin'){
                View::select('pre_orden_admin');
      }else{
        View::select('pre_orden');
      }
      $this->check = Input::post('check');    
      $this->dia = $_POST['reservacion']['dia'];
      $this->fecha_reservacion = $_POST['reservacion']['fecha_reservacion'];
    }
  }
  public function disponibilidadPaint()
  {
       if(!Session::has('fecha_reserva')){
      $date = new DateTime('now');
            $this->fecha_reservacion = $date->format('d-m-Y');
    }else{
      $this->fecha_reservacion = Session::get('fecha_reserva');
    }
    
    if(Input::hasPost('reservacion')){
      if($this->rol == 'admin'){
                View::select('pre_orden_admin_paint');
      }else{
        View::select('pre_orden_paint');
      }
      $this->check = Input::post('check');    
      $this->dia = $_POST['reservacion']['dia'];
      $this->fecha_reservacion = $_POST['reservacion']['fecha_reservacion'];
    }
  }
  /**
   * Paso de Pre Orden el usuario ve el total, horarios y canchas seleccionadas
   * @return unknown_type
   */
  public function pre_orden()
  {
    View::select(NULL);
    if(Input::post('reservacion')){
      $alquiler = new Alquiler();
      //Primero se verifica que el Cupon no exista
            // $cuponPost = $_POST['reservacion']['cupon'];
            $this->total = Input::post('totalPagar');
            $this->cupon = 'no';
            // if(Validate::isNull($cuponPost) != TRUE){
             //  if ($alquiler->cuponExiste($cuponPost) == TRUE){
             //      Flash::error('<p>El cupón no es válido o ya fue utilizado, por favor verique e intente nuevamente</p>');
             //      View::select('pre_orden');
             //      $this->check = Input::post('check');        
             //      $this->dia = $_POST['reservacion']['dia'];
             //      $this->fecha_reservacion = $_POST['reservacion']['fecha_reservacion'];
             //      return FALSE;
             //  }
            //  $this->total = $this->total-($this->total*0.20);
            //     $this->cupon = 'si';
            // }
            
            if($alquiler->nuevo(Input::post('reservacion'), Input::post('check'),false)){
                $this->alquiler = $alquiler;
                $this->digest = Digest::getDigest($this->total, $alquiler->id);
                View::select('formTDC');
                return;
            }else{
              if (Input::post('cancha')==4) 
              {
                  return Router::toAction('paint');
              }
              return Router::toAction('disponibilidad');
            }
            
    }else{
      if (Input::post('cancha')==4) 
      {
         return Router::toAction('paint');
      }
      return Router::toAction('disponibilidad');
    }
  }
  public function pre_orden_paint() 
  {

     View::select(NULL);
    if(Input::post('reservacion')){
      $alquiler = new Alquiler();
      //Primero se verifica que el Cupon no exista
            // $cuponPost = $_POST['reservacion']['cupon'];
            $this->total = Input::post('totalPagar');
            $this->cupon = 'no';
            
            // if(Validate::isNull($cuponPost) != TRUE){
             //  if ($alquiler->cuponExiste($cuponPost) == TRUE){
             //      Flash::error('<p>El cupón no es válido o ya fue utilizado, por favor verique e intente nuevamente</p>');
             //      View::select('pre_orden');
             //      $this->check = Input::post('check');        
             //      $this->dia = $_POST['reservacion']['dia'];
             //      $this->fecha_reservacion = $_POST['reservacion']['fecha_reservacion'];
             //      return FALSE;
             //  }
            //  $this->total = $this->total-($this->total*0.20);
            //     $this->cupon = 'si';
            // }
//var_dump($alquiler->nuevoPaint(Input::post('reservacion'), Input::post('check')));
            if($alquiler->nuevoPaint(Input::post('reservacion'), Input::post('check'))){
             
                $this->alquiler = $alquiler;
                $this->digest = Digest::getDigest($this->total, $alquiler->id);
                View::select('formTDC');
                return;
            }else{
              
              if (Input::post('cancha')==4) 
              {
                  return Router::toAction('paint');
              }
              return Router::toAction('disponibilidad');
            }
            
    }else{
      if (Input::post('cancha')==4) 
      {
         return Router::toAction('paint');
      }
      return Router::toAction('disponibilidad');
    }
  }
  public function getDigest(){

    View::template(null);
    $monto = Input::post('monto');
    $alquiler = Input::post('alquiler');
    
    $digest = Digest::getDigest($monto, $alquiler);
    return die($digest);
  }
  public function admintdc()
  {

    View::select('pretdc');
     
  }
  public function admintdcf(){
       $alquiler = new Alquiler();
       $alquiler->time=new Date();
       $alquiler->user_id = Input::post('userID');
       $alquiler->save();
       $value = Input::post('total');
       if (strrchr($value, ",")) {
         $value = str_replace(',','',$value );
         $value = intval($value) *10;
       }else{
        $value = str_replace('.','',$value );
        $value = intval($value) *100;
       }
       


       $this->digest = Digest::getDigest2($value, $alquiler->id);
       $this->total1 = $value;
       $this->total =Input::post('total');
       $this->cupon = 'no';
       $this->alquiler = $alquiler;
       View::select('formTDC-admin');
  }
  public function pagado()
  {
    View::select(NULL);
    Logger::warning("START", 'payment');
    Logger::warning($_POST['Respuesta'].' | '.$_POST['total'] .' | '.Session::get('userID').' | '. Input::post('orderid') , 'payment');
    if(isset($_POST['Respuesta'])){
      $alquiler = new Alquiler();
      $orderID = Input::post('orderid');
      $respuesta = Util::underscore($_POST['Respuesta']);
      if(in_array($respuesta, array('ABORTED', 'DENIED', 'WRONG_DIGEST'))){
        $alquiler->setEstatus($orderID, $respuesta);
        View::select('transNegada');
      }else if ($respuesta == 'APPROVED'){
        $this->refnum = Input::post('refnum');
        $digest = Digest::getDigestResponse(Input::post('total'), $this->refnum, $orderID);
        Logger::warning("Cheking digest -> $digest | ". Input::post('digest'), 'payment');

        if($digest == Input::post('digest')){
          Logger::warning("OK Digest", 'payment');
          $transaccion = new Transaccion();
          $transaccion->nuevo($this->refnum, Input::post('total'), $orderID);
            $alquiler->setEstatus($orderID, $respuesta);
            $this->orderID = $orderID;
            $this->reservacion = $alquiler->getReservacion($orderID, Session::get('userID'));
            if (sizeof($this->reservacion)==0) {
               $this->reservacion = $alquiler->getReservacionPaint($orderID, Session::get('userID'));
            }
            $this->cupon = Input::post('strvalue1');	
            $this->total = Input::post('total')/100 ;
            //enviando mail
            Mailer::recibo($this->reservacion, $orderID, $this->cupon, $this->refnum,Session::get('userID'), $this->total);
            View::select('factura');
        }
      }
    }
    Logger::warning("END", 'payment');
  }
  /**
   * Para efecto de pruebas
   * 
   * @return
   */
  public function factura($idAlquiler)
  {
    $alquiler = new Alquiler();
    $this->orderID = $idAlquiler;
    $this->cupon = 'si';
    $this->refnum = "000000000";
    $this->reservacion = $alquiler->getReservacion(23, 1932);
    Mailer::reciboClone($this->reservacion, $this->orderID, $this->cupon, $this->refnum,1932);
  }
  /**
   * REaliza el pre odern de los admines, esto porque ellos no pagan
   * TODO: falta agregar la descripcion
   * @return unknown_type
   */
  public function pre_orden_admin()
  {
      //En caso que sea rol ADMIN reserva sin pagar
        if($this->rol == 'admin'){
          $alquiler = new Alquiler();
            if($alquiler->nuevo(Input::post('reservacion'), Input::post('check'), 'APPROVED')){                    
                Flash::notice('<p>Reservación hecha</p>');
                return Router::toAction('disponibilidad');
            }
        }
  }
   public function pre_orden_admin_paint()
  {
      //En caso que sea rol ADMIN reserva sin pagar
        if($this->rol == 'admin'){
          $alquiler = new Alquiler();
            if($alquiler->nuevoPaint(Input::post('reservacion'), Input::post('check'), 'APPROVED')){                    
                Flash::notice('<p>Reservación hecha</p>');
                return Router::toAction('paint');
            }
        }
  }
  /**
   * Muestra informacion historica de los alquileres
   * @return unknown_type
   */
  public function mis_alquileres($page=1)
  {
        $alquiler = new Alquiler();
        $this->reservaciones = $alquiler->getAlquileres($page, Session::get('userID'));
        $this->reservacionesPaint = $alquiler->getAlquileresPaint($page, Session::get('userID'));
  }
  
    public function detalle_alquiler($idAlquiler)
    {
      View::template(NULL);
        $alquiler = new Alquiler();
        $this->identificador = $idAlquiler;
        $this->reservacion = $alquiler->getReservacion($idAlquiler, Session::get('userID'));
        if(empty($this->reservacion)){
            Flash::info('<p>Número de identificador no encontrado. Verifique e intente nuevamente</p>');
            return Router::toAction('index');
        }
        
        $this->alquiler = $alquiler->getAlquiler($idAlquiler);
    }
    public function detalle_alquilerPaint($idAlquiler)
    {
      View::template(NULL);
        $alquiler = new Alquiler();
        $this->identificador = $idAlquiler;
        $this->reservacion = $alquiler->getReservacionPaint($idAlquiler, Session::get('userID'));
        if(empty($this->reservacion)){
            Flash::info('<p>Número de identificador no encontrado. Verifique e intente nuevamente</p>');
            return Router::toAction('index');
        }
        
        $this->alquiler = $alquiler->getAlquiler($idAlquiler);
    }
}
