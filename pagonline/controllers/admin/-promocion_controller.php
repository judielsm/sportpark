<?php
Load::models('promocion');
class PromocionController extends AppController
{
	public function nuevo()
	{
		$this->horas = Load::model('hora')->dbSelect("hora", "hora_time");
		$this->dias = Load::model('dia')->dbSelect("nombre", 'id');
		$this->canchas = Load::model('cancha')->dbSelect("nombre", 'id');
		
		if(Input::hasPost('promocion')){
			$promocion = new Promocion();
			if($promocion->crear(Input::post('promocion'))){
				Flash::success('información guardada');
				Input::delete('promocion');
				Input::delete('promocion_hora');
				Input::delete('promocion_dia');
				Input::delete('promocion_cancha');
			}else{
				Flash::error('Ops! algo salio mal');
			}
		}
	}
	public function listar()
	{
		View::template('admin');
		$this->promocion = Load::model('promocion')->find("conditions: estatus=1");
	}
	
	public function editar($id)
	{
		$this->horas = Load::model('hora')->dbSelect("hora", "hora_time");
		$this->dias = Load::model('dia')->dbSelect("nombre", 'id');
		$this->canchas = Load::model('cancha')->dbSelect("nombre", 'id');
		if(Input::hasPost('promocion')){
			$promocion = new Promocion();
			if($promocion->crear(Input::post('promocion'))){
				Flash::success('información guardada');
			}else{
				Flash::error('Ops! algo salio mal');
			}
		}
		$this->promocion = Load::model('promocion')->find($id);
		$this->promocion_cancha = Load::model('promocion_cancha')->toArray($this->promocion->promocion_cancha, 'cancha_id');
		$this->promocion_hora = Load::model('promocion_hora')->toArray($this->promocion->promocion_hora, 'hora_id');
		$this->promocion_dia = Load::model('promocion_dia')->toArray($this->promocion->promocion_dia, 'dia_id');
	}
	
}