<?php
Load::models('alquiler');
class ReservacionesController extends AppController
{
	public function index()
	{
		View::select('myList');
	}
	public function listr($page=1){
		$alquiler = new Alquiler();
		$this->reservaciones = $alquiler->getAlquileres($page);
		View::select('index');
	}
	public function listPaint($page=1){
		$alquiler = new Alquiler();
		$this->reservaciones = $alquiler->getAlquileresPaint($page);
		View::select('listPaint');
	}
	public function detalle($idAlquiler, $status='APPROVED')
	{
		if(Input::isAjax()){
		  View::select('detalle.ajax', NULL);
		}
		$alquiler = new Alquiler();
		$this->identificador = $idAlquiler;
		$this->reservacion = $alquiler->getReservacion($idAlquiler, NULL, $status);
		if(empty($this->reservacion)){
			Flash::info('<p>Número de identificador no encontrado. Verifique e intente nuevamente</p>');
			return Router::toAction('index');
		}

		$this->alquiler = $alquiler->getAlquiler($idAlquiler, $status);
	}
	public function detallePaint($idAlquiler, $status='APPROVED')
	{
		if(Input::isAjax()){
		  View::select('detalle.ajax', NULL);
		}
		$alquiler = new Alquiler();
		$this->identificador = $idAlquiler;
		$this->reservacion = $alquiler->getReservacionPaint($idAlquiler, NULL, $status);
		if(empty($this->reservacion)){
			Flash::info('<p>Número de identificador no encontrado. Verifique e intente nuevamente</p>');
			return Router::toAction('listPaint');
		}

		$this->alquiler = $alquiler->getAlquiler($idAlquiler, $status);
	}
	/**
	 * Muestra las reservas para el día
	 * @return unknown_type
	 */
	public function hoy()
	{
		View::select('listHoy');
	}
	public function hoylist(){
		View::select('hoy');
		$alquiler = new Alquiler();
        $this->reservaciones = $alquiler->getAlquileresHoy();
	}
	public function hoyPaint(){
		View::select('hoyPaint');
		$alquiler = new Alquiler();
        $this->reservaciones = $alquiler->getAlquileresHoyPaint();
	}
	public function buscar()
	{
		if(Input::hasPost('identificador')){

			$identificador = filter_var(Input::post('identificador'), FILTER_SANITIZE_NUMBER_INT);
			if($identificador){
                return Router::toAction("detalle/$identificador");
			}else{
				$alquiler = new Alquiler();
				$this->reservaciones = $alquiler->search(Input::post('fechaAlquiler'), Input::post('cedula'));
				if($this->reservaciones==FALSE){
					Flash::notice('<p>No se encontraron resultados!</p>');
					return Router::toAction('index');
				}
				Input::delete();
				View::select('hoy');
			}

		}
	}
	public function buscarPaint()
	{
		if(Input::hasPost('identificador')){

			$identificador = filter_var(Input::post('identificador'), FILTER_SANITIZE_NUMBER_INT);
			if($identificador){
                return Router::toAction("detallePaint/$identificador");
			}else{
				$alquiler = new Alquiler();
				$this->reservaciones = $alquiler->searchPaint(Input::post('fechaAlquiler'), Input::post('cedula'));
				if($this->reservaciones==FALSE){
					Flash::notice('<p>No se encontraron resultados!</p>');
					return Router::toAction('listPaint');
				}
				Input::delete();
				View::select('hoyPaint');
			}

		}
	}
	public function cancelar($id_alquiler)
	{
		$alquiler = new Alquiler();
		if(Input::is('POST')){
			if($alquiler->setEstatus($id_alquiler, 'CANCELLED')){
				Flash::success('<p>Reservación cancelada</p>');
				return Router::toAction('index');
			}
		}else{
			$this->identificador = $id_alquiler;
			$this->reservacion = $alquiler->getReservacion($id_alquiler);
			if(empty($this->reservacion)){
				Flash::info('<p>Número de identificador no encontrado. Verifique e intente nuevamente</p>');
				return Router::toAction('index');
			}

			$this->alquiler = $alquiler->getAlquiler($id_alquiler);
		}

	}
	public function cancelarPaint($id_alquiler)
	{
		$alquiler = new Alquiler();
		if(Input::is('POST')){
			if($alquiler->setEstatus($id_alquiler, 'CANCELLED')){
				Flash::success('<p>Reservación cancelada</p>');
				return Router::toAction('listPaint');
			}
		}else{
			$this->identificador = $id_alquiler;
			$this->reservacion = $alquiler->getReservacionPaint($id_alquiler);
			if(empty($this->reservacion)){
				Flash::info('<p>Número de identificador no encontrado. Verifique e intente nuevamente</p>');
				return Router::toAction('listPaint');
			}

			$this->alquiler = $alquiler->getAlquiler($id_alquiler);
		}

	}
	public function canceladas()
	{
		View::select('canlist');
	}
	public function canceladasList($page=1)
	{
		View::select('canceladas');
		$alquiler = new Alquiler();
		$this->reservaciones = $alquiler->getAlquileres($page, NULL, 'CANCELLED');
	}
	public function canceladasPaint($page=1)
	{
		View::select('canceladasPaint');
		$alquiler = new Alquiler();
		$this->reservaciones = $alquiler->getAlquileresPaint($page, NULL, 'CANCELLED');
	}
	public function before_filter(){
		if(Session::get('rol') != 'admin'){
			return Router::redirect('/');
		}
	}
}
