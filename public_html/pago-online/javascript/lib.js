      $("#promocion_hora,#promocion_dia, #promocion_cancha").bsmSelect({
        addItemTarget: 'bottom',
        removeLabel: 'Eliminar'
      })
      
$("#fecha").dateinput({format:'dd-mm-yyyy', min: -1});
$("#fechaAlquiler").dateinput({format:'dd-mm-yyyy'});
      
//$('.table td').live('click', function(){$(this).toggleClass('sel') })

    /*function countChecked() {
      var n = $("input:checked").length;
      $("div").text(n + (n <= 1 ? " is" : " are") + " checked!");
    }
    countChecked();
    $(":checkbox").click(countChecked);*/


	//settings
	var fadeSpeed = 200, fadeTo = 0.5, topDistance = 30;
	var topbarME = function() { $('#nav').fadeTo(fadeSpeed,1); }, topbarML = function() { $('#nav').fadeTo(fadeSpeed,fadeTo); };
	var inside = false;
	//do
	$(window).scroll(function() {
		position = $(window).scrollTop();
		if(position > topDistance && !inside) {
			//add events
			topbarML();
			$('#nav').bind('mouseenter',topbarME);
			$('#nav').bind('mouseleave',topbarML);
			inside = true;
		}
		else if (position < topDistance){
			topbarME();
			$('#nav').unbind('mouseenter',topbarME);
			$('#nav').unbind('mouseleave',topbarML);
			inside = false;
		}
	});
	$.tools.validator.localize("es", {
		'*'			: 'Ingrese un valor correcto',
		':email'  	: 'Ingrese un email válido',
		':number' 	: 'Ingrese un valor numerico',
		':url' 		: 'Ingrese una URL válida',
		'[max]'	 	: 'Ingrese un valor menor que $1',
		'[min]'		: 'Ingrese un valor mayor que $1',
		'[required]'	: 'Campo requerido'
	});

	$(".form").validator({messageClass: 'messageClass', lang: 'es'});
	
	$(function() {

		// if the function argument is given to overlay,
		// it is assumed to be the onBeforeLoad event listener
		$("a[rel]").overlay({
			mask: {
				color: '#ebecff',
				loadSpeed: 200,
				opacity: 0.9
			},


			onBeforeLoad: function() {

				// grab wrapper element inside content
				var wrap = this.getOverlay().find(".contentWrap");

				// load the page specified in the trigger
				wrap.load(this.getTrigger().attr("href"));
			},
			onClose: function(){
				$('.contentWrap').replaceWith('<div class="contentWrap"></div>');
			}

		});
	});


$('.img-reservacion').click(function() {
	var cbox = $(this).parent().find('input[type=checkbox]')[0]
    cbox.checked = !cbox.checked;
        this.src = (cbox.checked)?"/img/reservado.png":"/img/disponible.png";
});
$('.img-paint').click(function() {
	var cbox = $(this).parent().find('input[type=checkbox]')[0]
    cbox.checked = !cbox.checked;
        this.src = (cbox.checked)?"/img/paint/reservado.png":"/img/paint/disponible.png";
});

/*$('#terminos').change(function() {
	if (this.checked) {
        $("#submit").attr('disabled', false).css("background-color","#E3E3E3");

    } else {
       $("#submit").attr('disabled', true).css("background-color","#a9a9aa");;
    }
});*/

$("#admintdc").submit(function(e){
	e.preventDefault();
	var form= this;
	$.post("/alquilar/getDigest", {monto:this.total.value  , alquiler: this.orderId.value}, function(res){
		$("#digest").val(res);
		form.submit();
	});
	//this.submit();
})

new Cleave('#my-input', {
    numeral: true,
    numeralDecimalMark: ',',
    delimiter: '.'
});
//fin de archivo