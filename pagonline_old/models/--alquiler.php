<?php
class Alquiler extends ActiveRecord
{	
	public function nuevo($dataReservacion, $checks, $status='PROCESS')
	{
		//por defecto en estatus procesando
		$this->estatus = $status;
		$this->time = date('Y-m-d H:i:s');
		$this->user_id = $dataReservacion['userID'];
		// $this->cupon = $dataReservacion['cupon'];
		$this->observacion = $dataReservacion['observacion'];
		try {
			$this->save();
			//Guarda las reservaciones
      //var_dump($checks);exit;
			foreach ($checks as $cancha=>$horas){
				Load::models('reservacion');
				foreach ($horas as $hora=>$precio){
					$reservacion = new Reservacion();
					$reservacion->cancha_id = $cancha;
					$reservacion->hora_id = $hora;
					$reservacion->precio = $precio;
					$reservacion->dia_id = $dataReservacion['dia'];
					$reservacion->fecha_reservacion = $dataReservacion['fecha_reservacion'];
					$reservacion->alquiler_id = $this->id;
					if($reservacion->isReservado($hora, $reservacion->dia_id, $reservacion->fecha_reservacion, $reservacion->cancha_id) == TRUE || !$reservacion->save()){
						$reservacion->delete_all("alquiler_id=$this->id");
						$this->delete();
						Flash::error("<p>Disculpe, alguna de las horas y canchas seleccionadas para la fecha $reservacion->fecha_reservacion ya se encuentran reservadas. Verifique su reservación e intente nuevamente</p>");
						return FALSE;
					}
				}
				//var_dump($cancha, $horas);
			}
			return TRUE;
		} catch (Exception $e) {
			//Flash::error($e->getMessage());
                        Logger::warning($e->getMessage(), 'alquiler');
		}
	}
    /**
     * Obtiene la informacion de la reservacion junto con sus precios, cancha, dia y horas
     * @return unknown_type
     */
    public function getReservacion($idAlquiler, $userID=NULL, $estatus='APPROVED')
    {
    	$user = NULL;
    	if($userID){
    		$user = "AND a.user_id=%d";
    	}
        $sql = "SELECT DISTINCT(h.hora),c.nombre as 'nombre_cancha', r.fecha_reservacion, h.hora FROM alquiler a 
                INNER JOIN reservacion as r ON r.alquiler_id=a.id 
                INNER JOIN dia as d ON d.id = r.dia_id
                INNER JOIN hora as h ON h.id = r.hora_id
                INNER JOIN cancha as c ON c.id = r.cancha_id
                INNER JOIN promocion_hora as ph ON ph.hora_id=r.hora_id
                INNER JOIN promocion_dia as pd ON pd.dia_id=r.dia_id
                INNER JOIN promocion_cancha as pc ON pc.cancha_id=r.cancha_id
                INNER JOIN promocion as p ON (p.id=pc.promocion_id AND p.id=pd.promocion_id AND p.id=ph.promocion_id)
                WHERE a.id=%d AND a.estatus='%s' $user";

        return $this->find_all_by_sql(sprintf($sql, $idAlquiler, $estatus, $userID));
    }
	/**
	 * Cambia el estatus del alquiler (PROCESS (valor por defecto), ABORTED, APPROVED, DENIED, WRONG_DIGEST, CANCELLED)
	 * @param $idAlquiler
	 * @return unknown_type
	 */
	public function setEstatus($idAlquiler, $estatus)
	{
		$this->find($idAlquiler);
		$this->estatus = $estatus;
		try {
			$this->save();
			return TRUE;
		} catch (Exception $e) {
			return FALSE;
		}
	}
	/**
	 * Verifica que el cupon exista o no
	 * @param $cupon
	 * @return unknown_type
	 */
	public function cuponExiste($cupon)
	{
		$cupon = filter_var($cupon, FILTER_SANITIZE_STRING);
		if(Validate::between($cupon, 14, 14) == TRUE){
			return (bool) $this->exists("cupon='$cupon'");
		}
		return TRUE;
	}
	/**
	 * Obtiene todos los alquileres
	 * @return unknown_type
	 */
	public function getAlquileres($page=1, $userID=NULL, $status='APPROVED')
	{
		$user = NULL;
		$sql = "SELECT DISTINCT(a.id), u.name, a.fecha_at, a.observacion, r.fecha_reservacion FROM alquiler a
                INNER JOIN user as u ON u.id=a.user_id
                INNER JOIN reservacion as r ON r.alquiler_id=a.id
                WHERE a.estatus='%s' AND r.fecha_reservacion >= CURRENT_DATE() %s ORDER BY r.fecha_reservacion ASC";
		//Agrega una condicion
		if($userID != NULL){
			$user = " AND a.user_id=$userID";
		}
		return $this->paginate_by_sql(sprintf($sql,$status,$user), 'per_page: 20', "page: $page");
	}
	/**
	 * Informacion del alquiler
	 * @param $idAlquiler
	 * @return unknown_type
	 */
	public function getAlquiler($idAlquiler, $status='APPROVED')
	{
		$sql = "SELECT a.id, u.name, a.cupon,a.observacion FROM alquiler a
                INNER JOIN user as u ON u.id=a.user_id
                WHERE a.estatus='%s' AND a.id=%d";
		return $this->find_by_sql(sprintf($sql, $status, $idAlquiler));
	}
	
    /**
     * Obtiene todos los alquileres del día
     * @return unknown_type
     */
    public function getAlquileresHoy($fecha=NULL, $status='APPROVED')
    {
    	if(!$fecha){
    		$fecha = date('Y-m-d');
    	}
        $sql = "SELECT DISTINCT(h.hora), a.id, c.nombre as 'nombre_cancha', r.fecha_reservacion, p.precio, h.hora, u.name, 
                u.usertype, ui.phone_1, ui.phone_2, a.observacion, ui.cedula, a.cupon FROM alquiler a 
                INNER JOIN reservacion as r ON r.alquiler_id=a.id 
                INNER JOIN dia as d ON d.id = r.dia_id
                INNER JOIN hora as h ON h.id = r.hora_id
                INNER JOIN cancha as c ON c.id = r.cancha_id
                INNER JOIN promocion_hora as ph ON ph.hora_id=r.hora_id
                INNER JOIN promocion_dia as pd ON pd.dia_id=r.dia_id
                INNER JOIN promocion_cancha as pc ON pc.cancha_id=r.cancha_id
                INNER JOIN promocion as p ON (p.id=pc.promocion_id AND p.id=pd.promocion_id AND p.id=ph.promocion_id)
                LEFT JOIN user_info as ui ON a.user_id=ui.user_id
                INNER JOIN user as u ON a.user_id=u.id
                WHERE a.estatus='%s' AND r.fecha_reservacion='$fecha' ORDER BY a.id ASC";
        return $this->find_all_by_sql(sprintf($sql,$status));
    }
    /**
     * Busqueda
     * @return unknown_type
     */
    public function search($fecha=NULL, $cedula=NULL, $status='APPROVED')
    {
    	$conditions = NULL;
        if($fecha){
            $date = strftime("%Y-%m-%d", strtotime($fecha));
            $conditions = " AND r.fecha_reservacion='$date'";
        }
        if($cedula){
        	$cedula = filter_var($cedula, FILTER_SANITIZE_NUMBER_INT);
        	$conditions .= " AND ui.cedula='$cedula'";
        }
        
        if($conditions === NULL){
        	return FALSE;
        }
        
    	$sql = "SELECT DISTINCT(h.hora), a.id, c.nombre as 'nombre_cancha', r.fecha_reservacion, p.precio, u.name, 
                u.usertype, ui.phone_1, ui.phone_2, a.observacion, ui.cedula, a.cupon FROM alquiler a 
                INNER JOIN reservacion as r ON r.alquiler_id=a.id 
                INNER JOIN dia as d ON d.id = r.dia_id
                INNER JOIN hora as h ON h.id = r.hora_id
                INNER JOIN cancha as c ON c.id = r.cancha_id
                INNER JOIN promocion_hora as ph ON ph.hora_id=r.hora_id
                INNER JOIN promocion_dia as pd ON pd.dia_id=r.dia_id
                INNER JOIN promocion_cancha as pc ON pc.cancha_id=r.cancha_id
                INNER JOIN promocion as p ON (p.id=pc.promocion_id AND p.id=pd.promocion_id AND p.id=ph.promocion_id)
                LEFT JOIN user_info as ui ON a.user_id=ui.user_id
                INNER JOIN user as u ON a.user_id=u.id
                WHERE a.estatus='%s' $conditions ORDER BY a.id ASC";
    	return $this->find_all_by_sql(sprintf($sql,$status));
    	
    }
}
