<?php
class Reservacion extends ActiveRecord
{
	/**
	 * Verifica la disponibilidad de una cancha
	 * 
	 * @param $horaID
	 * @param $diaID
	 * @param $canchaID
	 * @param $fecha fecha reservacion
	 * @return bool
	 */
    public function isReservado($horaID, $diaID, $fecha, $canchaID)
    {
    	$sql = 'SELECT COUNT(r.alquiler_id) as "reservado" FROM reservacion r
                INNER JOIN alquiler as a ON a.id=r.alquiler_id
                WHERE (a.estatus="APPROVED" OR (a.estatus="PROCESS" AND TIMESTAMPDIFF(MINUTE,a.time,now())<3)) 
                AND r.hora_id=%d AND r.dia_id=%d AND r.fecha_reservacion="%s" AND r.cancha_id=%d';
 
        if($this->find_by_sql(sprintf($sql, $horaID, $diaID, $fecha, $canchaID))->reservado >= 1){
        	return TRUE;
        }
        return FALSE;
    }
}
