<?php
class YQL
{
    private $_key = NULL;
    protected $_connection = NULL;
    /**
     * URL de la petición al WebService de YQL
     */
    private static $_url = 'http://query.yahooapis.com/v1/public/yql?format=json&q=';
    /**
     * Realiza una búsqueda sobre el Flickr
     * 
     * @param string $text
     * @param int $limit
     * @return stdClass
     */
    public static function search ($text, $limit=10)
    {
        $yql = "SELECT * FROM flickr.photos.search WHERE text='$text' LIMIT $limit";
        self::$_url .= urlencode($yql);
        $data = json_decode(self::_connect(self::$_url));
        //hay información?
        if($data->query->count){
            return $data->query->results->photo;
        }
        return FALSE;
    }
    /**
     * 
     */
    public static function feed()
    {
        $yql = "SELECT * FROM feed WHERE 
                    url='http://api.flickr.com/services/feeds/photos_public.gne?id=16933812@N08&lang=es-us&format=rss_200' 
                    LIMIT 2";
        self::$_url .= urlencode($yql);
        $data = json_decode(self::_connect(self::$_url));
        if($data->query->count){
            return $data->query->results->item;
        }
        return FALSE;
    }
    /**
     * Realiza la conexión CURL
     * @param String $url
     */
    private static function _connect ($url)
    {
        $c = curl_init();
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($c);
        curl_close($c);
        return $data;
    }
    
}