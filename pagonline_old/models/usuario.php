<?php
Load::models('info_usuario');
class Usuario extends ActiveRecord
{
	public function login($login, $pass)
	{
		
		$login = filter_var($login, FILTER_SANITIZE_STRING);
		if($this->find_first("login='$login' AND clave='$pass'")){
			$info = new InfoUsuario();
			$info->find($this->info_usuario_id);
			Session::set('userID', $this->id);
			Session::set('nombre', $info->nombre);
			Session::set('apellido', $info->apellido);
			return $this;
		}
		return FALSE;
	}
	public function before_save()
	{
		$this->clave = hash('md5',$this->login.$this->clave);
	}
}