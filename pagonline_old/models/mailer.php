<?php
/**
 * KBlog - KumbiaPHP Blog
 * PHP version 5
 * LICENSE
 *
 * This source file is subject to the GNU/GPL that is bundled
 * with this package in the file docs/LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to deivinsontejeda@gmail.com so we can send you a copy immediately.
 *
 * @author Deivinson Tejeda <deivinsontejeda@gmail.com>
 */
class Mailer
{
    //Atributos para el envio de correo (acceso privado)
    private static $_userName = 'no-reply@sportpark.com.ve'; // GMAIL username
    private static $_password = ''; // GMAIL password
    private static $_from = 'no-reply@sportpark.com.ve';
    /**
     * Quien envia el Mail
     * @var $fromName
     */
    public static $fromName = 'DIRECTV Sport Park';
    /**
     * Asunto del Mail
     * @var $subject
     */
    public static $subject = 'Comprobante de pago';
    
    public static function send ($correo, $person, $body)
    {
        //Cargamos las librería PHPMailer
        Load::lib('phpmailer');
        //instancia de PHPMailer
        $mail = new PHPMailer();
        //$mail->SMTPDebug = TRUE;
        $mail->IsSMTP();
        $mail->SMTPAuth = FALSE; // enable SMTP authentication
        $mail->CharSet = 'UTF-8';
        $mail->Host = 'localhost'; // sets GMAIL as the SMTP server
        $mail->Port = 25; // set the SMTP port for the GMAIL server
        $mail->Username = self::$_userName;
        //$mail->Password = self::$_password;
        $mail->AddReplyTo(self::$_from, self::$fromName);
        $mail->From = self::$_from;
        $mail->FromName = self::$fromName;
        $mail->Subject = self::$subject;
        $mail->Body = $body;
        $mail->WordWrap = 50; // set word wrap
        $mail->AddAddress($correo, $person);    
        $mail->MsgHTML($body);
        $mail->IsHTML(true); // send as HTML
        $exito = $mail->Send();
        $intentos = 1;
        //esto se realizara siempre y cuando la variable $exito contenga como valor false
        while ((! $exito) && $intentos < 1) {
            sleep(5);
            $exito = $mail->Send();
            $intentos = $intentos + 1;
        }
        return $exito;
    }
    /**
     * Envia el mail de recibo
     * @return unknown_type
     */
    public static function recibo($reservacion, $orderID, $cupon,$refnum, $userID)
    {
    	$user = Load::model('user')->find($userID);
    	$contentMail = View::bufferedPartial ( 'mail', array ('reservacion' => $reservacion, 'orderID'=>$orderID, 'cupon'=>$cupon, 'refnum'=>$refnum));
        if (self::send ( $user->email, $user->name, $contentMail)) {
            Flash::notice("<p>Ha sido enviado un correo electrónico, con la información que se muestra a continuación.</p>");
        } else {
        	Logger::error("FALLO ENVIO MAIL|$userID|$user->email", 'mail-error');
            Flash::error ('<p>Algo salió mal al enviar el correo electrónico.</p>');
        }
    }
    
    public static function reciboClone($reservacion, $orderID, $cupon,$refnum, $userID)
    {
    	$user = Load::model('user')->find($userID);
    	$contentMail = View::bufferedPartial ( 'mail', array ('reservacion' => $reservacion, 'orderID'=>$orderID, 'cupon'=>$cupon, 'refnum'=>$refnum));
        if (self::send ( 'deivinsontejeda@gmail.com', 'Deivinson Tejeda', $contentMail)) {
            Flash::notice("<p>Ha sido enviado un correo electrónico, con la información que se muestra a continuación.</p>");
        } else {
        	Logger::error("FALLO ENVIO MAIL|$userID|$user->email", 'mail-error');
            Flash::error ('<p>Algo salió mal al enviar el correo electrónico.</p>');
        }
    }
    public static function passRecovery($newPass, $user)
    {
    	$contentMail = View::bufferedPartial ( 'password', array ('newPass' => $newPass, 'user'=>$user));
    	self::$subject = 'Datos de Acceso';
        if (self::send ( $user->email, $user->name, $contentMail)) {
            Flash::valid("<p>Hemos enviado un correo electrónico, con los datos de acceso.</p>");
        }
    	
    }
}
