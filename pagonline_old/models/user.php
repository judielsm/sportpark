<?php
Load::models('user_info', 'mailer');
class User extends ActiveRecord
{
    public function add($user)
    {
        try {
        	$this->infoUser = Input::post('info_user');
        	$this->name = $this->infoUser['first_name'].' '.$this->infoUser['last_name'];
        	$this->block = "0";
            $this->create($user);
            //Guardando user_info
            $userInfo = new UserInfo($this->infoUser);
            $userInfo->user_id = $this->id;
            if(!$userInfo->create()){
            	$this->delete();
            	return FALSE;
            }
            return TRUE;
            
        } catch (Exception $e) {
        	$this->delete();
            Logger::error($e->getMessage());
            return FALSE;
        }
    }
    public function before_save()
    {
    	$this->password = HelperUser::getCryptedPass($this->password);
    }
    
    public function cambiarClave($claveA, $claveN)
    {
    	$this->find(Session::get('userID'));
    	list($md5pass, $saltpass) = explode(':', $this->password);
    	if(HelperUser::getCryptedPass($claveA, $saltpass) == $this->password){
    		$this->password = $claveN;
            return $this->save();
    	}
    	return FALSE;	
    }
    
    public function recuperarPass($correo)
    {
    	if($this->exists("email='$correo'")){
    		$this->find_first("email='$correo'");
    		$this->password = $newPass = HelperUser::genRandomPassword();
    		if($this->save()){
    			Mailer::passRecovery($newPass, $this);
    		}
    	}else{
    		Flash::error('<p>El correo sumistrado no se encuentra registrado.</p>');
    	}
    	
    	return FALSE;
    }
}