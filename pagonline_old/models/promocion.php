<?php
class Promocion extends ActiveRecord
{
	public function initialize(){
		$this->has_many('promocion_dia');
		$this->has_many('promocion_hora');
		$this->has_many('promocion_cancha');
	}
	public function crear($values){
		
		try {
			return $this->save($values);
		} catch (Exception $e) {
			Flash::error($e->getMessage());
		}
	}
	
	public function after_save()
	{
		Load::models('promocion_hora', 'promocion_dia', 'promocion_cancha');
		
		foreach (Input::post('promocion_hora') as $pHora){
			$hora = new PromocionHora();
			$hora->promocion_id = $this->id;
			$hora->hora_id = $pHora;
			$hora->save();
		}
		foreach (Input::post('promocion_dia') as $pDia){
			$dia = new PromocionDia();
			$dia->promocion_id = $this->id;
			$dia->dia_id = $pDia;
			$dia->save();
		}
		foreach (Input::post('promocion_cancha') as $pCancha){
			$cancha = new PromocionCancha();
			$cancha->promocion_id = $this->id;
			$cancha->cancha_id = $pCancha;
			$cancha->save();
		}
		
	}
	/**
	 * Obtiene el precio dada la/las horas y el dia 
	 * 
	 * @param $horas
	 * @param $dia
	 * @return unknown_type
	 */
	public function getPrecio($horas, $dia, $cancha)
	{
		if(is_array($horas)){
			$horas = join(',',$horas);
		}
		if(is_array($dia)){
			$dia = join(',',$dia);
		}
		$sql = "SELECT p.precio, d.nombre as 'dia', h.hora, c.nombre,p.id, c.img FROM promocion p
			    INNER JOIN promocion_hora as ph ON ph.promocion_id=p.id
			    INNER JOIN promocion_dia as pd ON pd.promocion_id=p.id
			    INNER JOIN promocion_cancha as pc ON pc.promocion_id=p.id
			    INNER JOIN dia as d ON d.id = pd.dia_id
			    INNER JOIN hora as h ON h.id = ph.hora_id
			    INNER JOIN cancha as c ON c.id = pc.cancha_id
			    WHERE ph.hora_id=%d AND pd.dia_id=%d AND pc.cancha_id=%d AND p.estatus=1 LIMIT 1";
		return $this->find_by_sql(sprintf($sql, $horas, $dia, $cancha));
	}
	/**
	 * Verifica que una cancha en un determinado dia y hora tenga una promocion asociada
	 * @param $cancha
	 * @param $dia
	 * @param $hora
	 * @return unknown_type
	 */
	public function hasPromocion($cancha, $dia, $hora){
		$sql = "SELECT COUNT(DISTINCT(h.hora)) as 'count_result' FROM promocion_hora ph
			   LEFT JOIN promocion as p ON p.id = ph.promocion_id
			   LEFT JOIN promocion_dia as pd ON pd.promocion_id = ph.promocion_id
			   LEFT JOIN promocion_cancha as pc ON pc.promocion_id = ph.promocion_id
			   LEFT JOIN hora as h ON h.id = ph.hora_id
			   LEFT JOIN dia as d ON d.id = pd.dia_id
			   LEFT JOIN cancha as c ON c.id = pc.cancha_id
			   WHERE d.id=$dia AND c.id=$cancha AND h.id=$hora AND p.estatus=1";
		/*if($this->find_by_sql($sql)->count_result == 0){
			var_dump($sql);
		}*/
		return $this->find_by_sql($sql)->count_result;
	}
}

/**
 * Para traer promociones
 * SELECT DISTINCT(h.hora), p.nombre, p.precio, p.id as "promocion_id", d.nombre, c.nombre FROM promocion_hora ph
   LEFT JOIN promocion as p ON p.id = ph.promocion_id
   LEFT JOIN promocion_dia as pd ON pd.promocion_id = ph.promocion_id
   LEFT JOIN promocion_cancha as pc ON pc.promocion_id = ph.promocion_id
   LEFT JOIN hora as h ON h.id = ph.hora_id
   LEFT JOIN dia as d ON d.id = pd.dia_id
   LEFT JOIN cancha as c ON c.id = pc.cancha_id
   WHERE d.id=1 AND c.id=1 AND h.id=23 AND p.estatus=1
 */