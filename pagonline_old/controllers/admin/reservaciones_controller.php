<?php
Load::models('alquiler');
class ReservacionesController extends AppController
{
	public function index($page=1)
	{
		$alquiler = new Alquiler();
		$this->reservaciones = $alquiler->getAlquileres($page);
	}

	public function detalle($idAlquiler, $status='APPROVED')
	{
		if(Input::isAjax()){
		  View::select('detalle.ajax', NULL);
		}
		$alquiler = new Alquiler();
		$this->identificador = $idAlquiler;
		$this->reservacion = $alquiler->getReservacion($idAlquiler, NULL, $status);
		if(empty($this->reservacion)){
			Flash::info('<p>Número de identificador no encontrado. Verifique e intente nuevamente</p>');
			return Router::toAction('index');
		}

		$this->alquiler = $alquiler->getAlquiler($idAlquiler, $status);
	}
	/**
	 * Muestra las reservas para el día
	 * @return unknown_type
	 */
	public function hoy()
	{
		$alquiler = new Alquiler();
        $this->reservaciones = $alquiler->getAlquileresHoy();
	}

	public function buscar()
	{
		if(Input::hasPost('identificador')){

			$identificador = filter_var(Input::post('identificador'), FILTER_SANITIZE_NUMBER_INT);
			if($identificador){
                return Router::toAction("detalle/$identificador");
			}else{
				$alquiler = new Alquiler();
				$this->reservaciones = $alquiler->search(Input::post('fechaAlquiler'), Input::post('cedula'));
				if($this->reservaciones==FALSE){
					Flash::notice('<p>No se encontraron resultados!</p>');
					return Router::toAction('index');
				}
				Input::delete();
				View::select('hoy');
			}

		}
	}

	public function cancelar($id_alquiler)
	{
		$alquiler = new Alquiler();
		if(Input::is('POST')){
			if($alquiler->setEstatus($id_alquiler, 'CANCELLED')){
				Flash::success('<p>Reservación cancelada</p>');
				return Router::toAction('index');
			}
		}else{
			$this->identificador = $idAlquiler;
			$this->reservacion = $alquiler->getReservacion($id_alquiler);
			if(empty($this->reservacion)){
				Flash::info('<p>Número de identificador no encontrado. Verifique e intente nuevamente</p>');
				return Router::toAction('index');
			}

			$this->alquiler = $alquiler->getAlquiler($id_alquiler);
		}

	}

	public function canceladas($page=1)
	{
		$alquiler = new Alquiler();
		$this->reservaciones = $alquiler->getAlquileres($page, NULL, 'CANCELLED');
	}

	public function before_filter(){
		if(Session::get('rol') != 'admin'){
			return Router::redirect('/');
		}
	}
}
