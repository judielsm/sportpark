<?php
Load::model('user');
class LoginController extends AppController
{
	public function registro()
	{
		//View::select(NULL, 'login');
		View::template('new_template');
		if(Input::hasPost('info_user')){
			$user = new User();
            if($_SESSION['rsa_key'] === Input::post('rsa32_key') && $_POST['user']['password2'] === $_POST['user']['password'] && $user->add(Input::post('user'))){
            	Input::delete();
            	Flash::success('Usuario Creado');
            	return Router::redirect('/');
            }else{
            	Flash::error('<p>Error al crear usuario. Verifique los datos suministrados</p>
            	              <p>Nombre de Usuario o Correo Eletrónico ya existe.</p>');
							return Router::redirect('/');

            }
            //Limpiando password
            $_POST['user']['password2'] = $_POST['user']['password'] = NULL;
		}
	}

	public function cambiar_clave()
	{
		if(Input::hasPost('c_r_actual')){
			$user = new User();
			if(Input::post('c_nueva') == Input::post('c_r_actual') & $user->cambiarClave(Input::post('c_actual'),Input::post('c_nueva'))){
				Flash::success('<p>Clave actualizada!</p>');
				return Router::redirect('/');
			}else{
				Input::delete();
				Flash::error('<p>Error al cambiar la clave. Verifique e intente nuevamente.</p>');
			}
		}
	}

	public function recuperar_clave()
	{
		if(Input::hasPost('correo')){
			$user = new User();
			$user->recuperarPass(Input::post('correo'));
			return Router::redirect('/');
		}
	}
}
