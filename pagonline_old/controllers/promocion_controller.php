<?php
Load::models('promocion');
class PromocionController extends AppController
{
	public function nuevo()
	{
		$this->horas = Load::model('hora')->dbSelect("hora", "hora_time");
		$this->dias = Load::model('dia')->dbSelect("nombre", 'id');
		$this->canchas = Load::model('cancha')->dbSelect("nombre", 'id');
		
		if(Input::hasPost('promocion')){
			$promocion = new Promocion();
			if($promocion->crear(Input::post('promocion'))){
				Flash::success('información guardada');
			}else{
				Flash::error('Ops! algo salio mal');
			}
		}
		//var_dump($this->horas);die;
	}
	
}