<?php
class HelperUser 
{
   /**
     * Generate a random password
     *
     * @static
     * @param   int     $length Length of the password to generate
     * @return  string          Random Password
     */
    public static function genRandomPassword($length = 8)
    {
        $salt = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $len = strlen($salt);
        $makepass = '';
        mt_srand(10000000 * (double) microtime());

        for ($i = 0; $i < $length; $i ++) {
            $makepass .= $salt[mt_rand(0, $len -1)];
        }

        return $makepass;
    }
    
    public static function getCryptedPass($pass, $salt=NULL)
    {
    	if(!$salt){
    		$salt = self::genRandomPassword(32);
    	}
    	$pass = hash('md5', $pass.$salt);
    	return $pass.':'.$salt;
    	
    }
}