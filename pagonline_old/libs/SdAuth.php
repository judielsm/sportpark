<?php
define('SESSION_KEY', 'AxwkEzPsRY25112'); // change to your own key.
class SdAuth
{
    /**
     * Mensaje de Error
     *
     * @var String
     */
    private static $_error = NULL;
    /**
     * islogin
     *
     * @param void
     * @return ture/false
     */
    public static function isLogged ()
    {
        Load::lib('session');//carga la extension session
        if (Session::get(SESSION_KEY) == TRUE) {
            return TRUE;
        } else {
            //not yet logged in.
            // check
            if (isset($_POST['mode']) && $_POST['mode'] == 'auth_login') {
                //data was posted.
                return self::check(trim($_POST['user_login']), trim($_POST['pass_login']));
            } else {
                //You can't enter!
                return false;
            }
        }
    }
    /**
     * check
     *
     * @param $username
     * @param $password
     * @return true/false
     */
    public static function check ($username, $pass)
    {
    	Load::models('user', 'user_info');
        $username = filter_var($username, FILTER_SANITIZE_STRING);
        $user = new User();
        $username = addslashes($username);
        if($user->find_first("username='$username' AND block=0")){
        	list($md5pass, $saltpass) = explode(':', $user->password);
        	if(HelperUser::getCryptedPass($pass, $saltpass) == $user->password){
	            Session::set('userID', $user->id);
	            Session::set('nombre', $user->name);
	            Session::set('rol', $user->usertype);
	            Session::set(SESSION_KEY, TRUE);
                Session::set('MODAL', TRUE);
	            return TRUE;
        	}
        }
        $_POST = array();
        self::setError('Usuario o clave incorrecto.');
        Session::set(SESSION_KEY, FALSE);
        return FALSE;
    }
    /**
     * logout
     *
     * @param void
     * @return void
     */
    public static function logout ()
    {
        Load::lib('session');
        Session::set(SESSION_KEY, FALSE);
        Session::set('MODAL', FALSE);
        Session::delete('userID');
        Session::delete('nombre');
        Session::delete('rol');
        Session::delete('fecha_reserva');
    }
    /**
     * @return string
     */
    public static function getError ()
    {
        return self::$_error;
    }
    /**
     * @param string $_error
     */
    public static function setError ($error)
    {
        self::$_error = $error;
    }
}
