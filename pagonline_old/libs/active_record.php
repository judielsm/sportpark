<?php
/**
 * ActiveRecord
 *
 * Esta clase es la clase padre de todos los modelos
 * de la aplicacion
 *
 * @category Kumbia
 * @package Db
 * @subpackage ActiveRecord
 */

// Carga el active record
Load::coreLib('kumbia_active_record');

class ActiveRecord extends KumbiaActiveRecord 
{
    /**
	 * Obtiene un array a partir de un modelo, para ser
	 * utilizada en un select.
	 * 
	 * @param string $columna
	 * @param string $order
	 * @return array
	 */
	public function dbSelect($column='nombre', $order=null)
	{
	    $pk = $this->primary_key[0];
	    $select = "SELECT $pk,";
        $select.= ActiveRecord::sql_sanizite($column);
        if ($this->schema) {
            $select.= " FROM {$this->schema}.{$this->source}";
        } else {
            $select.= " FROM {$this->source}";
        }
        if($order) {
                $select .= " ORDER BY $order";
        } 
        $results = array();
	    foreach ($this->db->in_query_assoc($select) as $result){
	        $results[$result[$pk]] = $result[$column];
	    }
	    return $results;
    }
    
	public function toArray($obj, $field){
		$data = array();
		foreach($obj as $item){
			$data[] = $item->$field;
		}
		return $data;
	}
}
